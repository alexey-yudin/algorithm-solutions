import {klarna1} from "../../../src/qualified/klarna/klarna_1";

describe("Klarna 1", () => {
    it('should 6 chars', function () {
        expect(klarna1('12345')).toEqual('12345');
    });

    it('should do something', () => {
        expect(klarna1('4556364607935616')).toEqual('4###########5616');
    });

    it('should do', () => {
        expect(klarna1('4556-3646-0793-5616')).toEqual('4###-####-####-5616');
    });

    it('should', () => {
        expect(klarna1('64607935616')).toEqual('6######5616');
    });

    it('should', () => {
        expect(klarna1('4556-3451?2222_4543')).toEqual('4###-####?####_4543');
    });

    it('should', () => {
        expect(klarna1('6--32--')).toEqual('6--32--');
    });

    it('should', () => {
        expect(klarna1('Nananananananananananananananana#Batman!')).toEqual('Nananananananananananananananana Batman!');
    });
});
