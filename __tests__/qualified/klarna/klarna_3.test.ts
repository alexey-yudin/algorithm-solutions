import { klarna3, klarna_3 } from '../../../src/qualified/klarna/klarna_3';

describe('Klarna 3', () => {
  it('should ', function () {
    expect(klarna3('1 2 3')).toEqual(3);
    expect(klarna3('1 3 +')).toEqual(4);
    expect(klarna3('1 3 *')).toEqual(3);
    expect(klarna3('4 2 /')).toEqual(2);
  });

  it('should 2', function () {
    expect(klarna_3('1 2 3')).toEqual(3);
    expect(klarna_3('1 3 +')).toEqual(4);
    expect(klarna_3('1 3 *')).toEqual(3);
    expect(klarna_3('4 2 /')).toEqual(2);
  });
});
