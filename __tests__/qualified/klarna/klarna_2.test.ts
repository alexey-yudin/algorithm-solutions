import { klarna2, klarna_2 } from '../../../src/qualified/klarna/klarna_2';

describe('Klarna 2', () => {
  it('should', function () {
    expect(klarna2(0)).toEqual('0');
    expect(klarna2(1)).toEqual('1st');
    expect(klarna2(2)).toEqual('2nd');
    expect(klarna2(3)).toEqual('3rd');
    expect(klarna2(4)).toEqual('4th');
    expect(klarna2(5)).toEqual('5th');
    expect(klarna2(11)).toEqual('11th');
    expect(klarna2(12)).toEqual('12th');
    expect(klarna2(13)).toEqual('13th');
  });

  it('should work', () => {
    expect(klarna_2(0)).toEqual('0');
    expect(klarna_2(1)).toEqual('1st');
    expect(klarna_2(2)).toEqual('2nd');
    expect(klarna_2(3)).toEqual('3rd');
    expect(klarna_2(4)).toEqual('4th');
    expect(klarna_2(5)).toEqual('5th');
    expect(klarna_2(11)).toEqual('11th');
    expect(klarna_2(12)).toEqual('12th');
    expect(klarna_2(13)).toEqual('13th');
  });
});
