import { missingInteger } from '../../src/interviews/missing-integer';

describe('Missing Integer', () => {
  describe('With positive only starting from 1', () => {
    it('should return the smallest int not presented in the array', () => {
      const source = [1, 4, 3, 6];
      const expectedResult = 5;

      expect(missingInteger(source)).toEqual(expectedResult)
    });
  });

  describe('With positive starting at bigger than 1', () => {
    it('should return the smallest int not presented in the array', () => {
      const source = [1, 2, 3, 4, 6, 10];
      const expectedResult = 5;

      expect(missingInteger(source)).toEqual(expectedResult)
    });
  });

  describe('With a single element', () => {
    it('should return the smallest int not presented in the array', () => {
      const source = [4];
      const expectedResult = 1;

      expect(missingInteger(source)).toEqual(expectedResult)
    });
  });

  describe('With negative numbers', () => {
    it('should return 1', () => {
      const source = [-1, -4, -2];
      const expectedResult = 1;

      expect(missingInteger(source)).toEqual(expectedResult)
    });
  });
});
