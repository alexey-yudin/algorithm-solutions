import { reverseString } from '../../src/interview-preparation/reverse-string';

describe("Reverse String", () => {
  it("should return reversed string", () => {
    const sourceStr = 'abcdefg';
    const expectedResult = 'gfedcba';

    expect(reverseString(sourceStr)).toEqual(expectedResult);
  });
});
