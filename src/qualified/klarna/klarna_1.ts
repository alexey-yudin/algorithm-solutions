export function klarna_1(creditCard: string) {
  if (creditCard.length < 6) {
    return creditCard;
  }

  const head = creditCard[0];
  const body = creditCard.slice(1, creditCard.length - 4);
  const tail = creditCard.slice(creditCard.length - 4, creditCard.length);

  const maskedBody = body.replace(/#/g, ' ')
    .split('')
    .map(char => isNaN(parseInt(char)) || char === ' ' ? char : '#')
    .join('');

  return `${head}${maskedBody}${tail}`;
}

export function klarna1(creditCard: string) {
  if (creditCard.length < 6) {
    return creditCard;
  }

  const tailStart = creditCard.length - 4;

  const head = creditCard[0];
  const tail = creditCard.slice(tailStart, creditCard.length);

  const digitRegex = /\d/;
  let body = creditCard.slice(1, tailStart).replace('#', ' ');
  while (body.match(digitRegex)) {
    body = body.replace(digitRegex, '#');
  }

  return `${head}${body}${tail}`;
}
