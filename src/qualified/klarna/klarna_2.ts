export function klarna2(n) {
  if (n === 0) {
    return `0`;
  }

  let suffix = 'th';
  const lastDigit = n % 10;
  const lastTwoDigit = n % 100;

  switch (lastDigit) {
    case 1:
      suffix = 'st';
      break;
    case 2:
      suffix = 'nd';
      break;
    case 3:
      suffix = 'rd';
      break;
  }

  if (lastTwoDigit === 11 || lastTwoDigit === 12 || lastTwoDigit === 13) {
    suffix = 'th';
  }

  n = `${n}${suffix}`;

  return n;
}

export function klarna_2(n) {
  if (n === 0) {
    return '0';
  }

  let suffix: string;
  const lastDigit = n % 10;
  const lastTwoDigit = n % 100;

  switch (lastDigit) {
    case 1:
      suffix = 'st';
      break;
    case 2:
      suffix = 'nd';
      break;
    case 3:
      suffix = 'rd';
      break;
    default:
      suffix = 'th';
      break;
  }

  if (lastTwoDigit >= 11 && lastTwoDigit <= 13) {
    suffix = 'th';
  }

  return `${n}${suffix}`;
}
