export function klarna3(expresion: string) {
  const operators = {
    '+': (x, y) => x + y,
    '-': (x, y) => x - y,
    '*': (x, y) => x * y,
    '/': (x, y) => x / y
  };

  let stack = [];

  expresion.split(' ').forEach((token) => {
    if (token in operators) {
      let [y, x] = [stack.pop(), stack.pop()];
      stack.push(operators[token](x, y));
    } else {
      stack.push(parseFloat(token));
    }
  });

  return stack.pop();
}

export function klarna_3(expresion: string) {
  const operators = {
    '+': (x, y) => x + y,
    '-': (x, y) => x - y,
    '*': (x, y) => x * y,
    '/': (x, y) => x / y,
  };

  return expresion.split(' ')
    .reduce((stack, token) => {
      if (token in operators) {
        const y = stack.pop();
        const x = stack.pop();
        stack.push(operators[token](x, y));
      } else {
        stack.push(parseFloat(token));
      }

      return stack;
    }, [])
    .pop();
}
