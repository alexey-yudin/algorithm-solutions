class Node {
  constructor(data) {
    this.data = data;
    this.children = [];
  }

  add(data) {
    this.children.push(new Node(data));
  }

  remove(data) {
    this.children = this.children.filter(i => i === data);
  }

  displayAllChildren() {
    this.children.forEach(i => console.log(i));
  }
}

class Tree {
  constructor() {
    this.root = null;
  }

  addRoot(data) {
    if (this.root) {
      return;
    }

    this.root = new Node(data);
  }

  traverseBF(fn) {
    let arr = [this.root];

    while (arr.length > 0) {
      const first = arr.shift();
      arr = arr.concat(first.children);
      fn(first);
    }
  }

  traverseDF(fn) {
    let arr = [this.root];

    while (arr.length > 0) {
      const first = arr.shift();
      arr = first.children.concat(arr);
      fn(first);
    }
  }
}

const tree = new Tree();
tree.addRoot(4);
tree.root.add(6);
tree.root.add(7);
tree.root.add(8);

tree.traverseBF(node => {
  if (node.data === 7) {
    node.children.push(new Node(14));
    node.children.push(new Node(15));
  }
});

function levelWidth(root) {
  const stopperSign = 's';
  const counters = [0];
  let arr = [root, stopperSign];

  while (arr.length > 1) {
    const node = arr.shift();

    if (node === stopperSign) {
      counters.push(0);
      arr.push(stopperSign);
    } else {
      arr = arr.concat(node.children);
      counters[counters.length - 1] += 1;
    }
  }

  return counters;
}

console.log(levelWidth(tree.root));
