/**
 * A genetic algorithm is based in groups of chromosomes,
 * called populations. To start our population of chromosomes
 * we need to generate random binary strings with a specified length.
 * In this kata you have to implement a function generate that receives
 * a length and has to return a random binary strign with length characters.
 */
export function generate(length: number): string {
  return Array(length).fill(null).map(() => Math.floor(Math.random() * 2)).join('');
}
