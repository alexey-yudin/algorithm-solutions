function restoreIp(S) {
    const length = S.length - 3;
    let count = 0;

    for (let i = 0; i < length; i++) {
        for (let j = 0; j < length - i; j++) {
            for (let l = 0; l < length - i - j; l++) {
                let isValid = true;
                const splittedIp = S.toString().split('');

                splittedIp.splice(i + 1, 0, ".");
                splittedIp.splice(i + j + 3, 0, ".");
                splittedIp.splice(i + j + l + 5, 0, ".");

                for (const item of splittedIp.join('').split('.')) {
                    if (parseInt(item, 10) > 255) {
                        isValid = false;
                        break;
                    }
                }

                if (isValid) {
                    count += 1;
                }
            }
        }
    }

    return count;
}
