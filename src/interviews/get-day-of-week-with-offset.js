export function getDayOfWeekWithOffset(S, K) {
    const amountOfDaysInTheWeek = 7;
    const dayOffset = K % amountOfDaysInTheWeek;

    const daysMap = {
        'Mon': 0,
        'Tue': 1,
        'Wed': 2,
        'Thu': 3,
        'Fri': 4,
        'Sat': 5,
        'Sun': 6
    };

    const day = (daysMap[S] + dayOffset) % amountOfDaysInTheWeek;
    return Object.keys(daysMap)[day];
}
